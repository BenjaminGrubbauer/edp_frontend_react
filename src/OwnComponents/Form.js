import React from 'react';

class IssueForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      issueName: 'default',
      issueDescription: 'default'
    };


    this.handleChange_issueName = this.handleChange_issueName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange_issueName(event) {
    this.setState({ issueName: event.target.value });
  }

  handleChange_issueDescription = event => {
    this.setState({ issueDescription: event.target.value })
  }

  handleSubmit(event) {
    alert('An Issue with the following name was submitted: ' + this.state.issueName +"\n"
      + 'Issue Description is: ' + this.state.issueDescription);

    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Issue:
            <input type="text" value={this.state.issueName} onChange={this.handleChange_issueName} />
        </label>
        <input type="submit" value="Submit" />
        <div>
          <label>Description</label>
          <textarea value={this.state.issueDescription} onChange={this.handleChange_issueDescription}></textarea>
        </div>
      </form>
    );
  }
}

export default IssueForm;